import { Column, Entity, Index, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';

export type RuleType = 'Четные' | 'Нечетные' |
    'Повтор' | 'Не выпало' |
    'Одинаковых <' | 'Одинаковых >' |
    'Есть соседи' | 'Нет соседей' |
    'MIN <' | 'MIN >' |
    'MAX <' | 'MAX >' |
    'Сумма <' | 'Сумма >' |
    'Есть кратный' | 'Нет кратного' |
    'MIN четный' | 'MAX четный' | 'MIN нечетный' | 'MAX нечетный' |
    'Разность >' | 'Разность <' |
    //Rapido only
    'Четный (бонус)' | 'Нечетный (бонус)' | 'Повтор (бонус)' | 'Не выпало (бонус)';

export type LotteryType = 'rapido' | '12x24' | 'duel' | 'top3' | 'keno' | 'joker' | 'bingo75' | '6x45' | '5x50' | '5x36plus' | '4x20' | 'quickgames' | 'gzhl' | '6x36' | 'ruslotto' | 'zp' | '7x49';

export type Lotery = {
    id: number,
    date: Date,
    numbers: number[]
}

@Access({ _: 0 }, { display: 'table', icon: 'bell', order: 10 })
@Entity()
export class Rule {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        enum: ['Четные', 'Нечетные',
            'Повтор', 'Не выпало',
            'Одинаковых <', 'Одинаковых >',
            'Есть соседи', 'Нет соседей',
            'MIN <', 'MIN >',
            'MAX <', 'MAX >',
            'Сумма <', 'Сумма >',
            'Есть кратный', 'Нет кратного',
            'MIN четный', 'MAX четный', 'MIN нечетный', 'MAX нечетный',
            'Разность >', 'Разность <',
            'Четный (бонус)', 'Нечетный (бонус)', 'Повтор (бонус)', 'Не выпало (бонус)'
        ], default: ''
    })
    type: RuleType;

    @Column({ type: 'int', unsigned: true, default: 0 })
    value: number;

    @v({ min: 1, sendAlways: true })
    @Column({ type: 'int', unsigned: true, default: 2 })
    count: number;

    @v({ sendAlways: true })
    @Index()
    @Column({ enum: ['rapido', '12x24', 'duel', 'top3', 'keno', 'joker', 'bingo75', '6x45', '5x50', '5x36plus', '4x20', 'quickgames', 'gzhl', '6x36', 'ruslotto', 'zp', '7x49'], default: '' })
    lottery: LotteryType;

    @v({ readonly: true })
    @Column({ type: 'int', unsigned: true, default: 0 })
    lastProcessedDraw: number;
}

export abstract class RuleRepository extends Repository<Rule>{ }