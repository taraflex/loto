import { Column, Repository } from 'typeorm';

import { CoreSettings } from '@entities/CoreSettings';
import { SingletonEntity } from '@ioc';

@SingletonEntity()
export class Settings extends CoreSettings {
    @Column({ default: 0 })
    admin: number;
}

export abstract class SettingsRepository extends Repository<Settings>{ }