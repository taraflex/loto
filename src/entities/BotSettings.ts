import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';

const $1DAY = 60 * 60 * 24;

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'cog', order: 4 })
@SingletonEntity()
export class BotSettings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ pattern: /^\d{9}:[\w-]{35}$/, description: 'Токен телеграм бота [ℹ️ справка](https://www.youtube.com/watch?v=W0f66uie9e8)' })
    @Column({ default: '' })
    telegramBotToken: string;

    @v({
        max: $1DAY, positive: true, description: `Минимальный интервал между запросами к сайту в секундах.   
Url результатов тиражей опрашиваются циклично.   
Чем больше различных url проверяем, тем меньше следует установить, чтобы соблюсти своевременность оповещений.` })
    @Column({ type: 'int', default: 60, unsigned: true })
    minimumUpdateInterval: number;
}

export abstract class BotSettingsRepository extends Repository<BotSettings>{ }