import * as cheerio from 'cheerio';
import { Inject, Singleton } from 'typescript-ioc';

import { BotSettings } from '@entities/BotSettings';
import { LotteryType, Rule, RuleRepository, RuleType } from '@entities/Rule';
import { promiseRequest } from '@utils/cancelable-request';
import { fastDelay } from '@utils/delay';

import { Bot } from './Bot';

type Interval = { min: number, max: number };

const MAX_PROCESSED_LOTTERIES = 100;
const INTERVALS: Readonly<{ [_ in LotteryType]?: Interval }> = Object.freeze({
    'top3': { min: 0, max: 9 },
    'rapido': { min: 1, max: 20 },
    'duel': { min: 1, max: 26 },
    '12x24': { min: 1, max: 24 },
    'keno': { min: 1, max: 80 },
});

function last<T>(a: T[]) {
    return a[a.length - 1];
}

function addOne(v) {
    return v > 0 ? v + 1 : 0;
}

function table(a: string[]) {
    let r = '\n';
    for (let i = 0; i < a.length; ++i) {
        r += a[i].padEnd(13) + ((i + 1) % 3 === 0 ? '\n' : '');
    }
    return r;
}

function formatType(t: LotteryType) {
    switch (t) {
        case 'top3': return '🥉top3';
        case 'rapido': return '®rapido';
        case 'duel': return '🥊duel';
        case '12x24': return '⚡️12x24';
        case 'keno': return '📽keno';
    }
    return t;
}

function* calc(this: (a: number[], n: number) => boolean, draws: number[], dict: Map<number, number[]>, { count, value, lastProcessedDraw }: Rule, _: Interval) {
    let c = 0;
    let groupStart = 0;
    for (let draw of draws) {
        if (!c) {
            groupStart = draw;
        }
        const ns = dict.get(draw);
        if (this(ns, value)) {
            ++c;
        } else {
            if (c >= count) {
                yield { draw: groupStart, data: '`' + c + '`' };
            }
            c = 0;
        }
    }
    if (c >= count && groupStart > lastProcessedDraw) {
        yield { draw: groupStart, data: '`' + c + '`' };
    }
}

function* accCalc(this: Function, draws: number[], dict: Map<number, number[]>, { count, lastProcessedDraw }: Rule, interval: Interval) {
    if (count > 1 && interval) {
        for (let i = 0; i < draws.length - count; ++i) {
            if (draws[i] <= lastProcessedDraw) {
                break;
            }
            const storage: { [_: number]: number } = {};
            for (let k = interval.min; k <= interval.max; ++k) {
                for (let j = i; j < draws.length; ++j) {
                    if (this(dict.get(draws[j]), k)) {
                        storage[k] = (storage[k] | 0) + 1
                    } else {
                        break;
                    }
                }
            }
            const result = [];
            for (let k in storage) {
                const v = storage[k];
                if (v >= count) {
                    result.push(`|${k}| - \`${v.toString().padEnd(3)}\``);
                }
            }
            if (result.length > 0) {
                yield { draw: draws[i], data: result.length > 1 ? table(result) : result[0] };
            }
        }
    }
}

function Accumulator(a: number[], k: number) {
    return a.includes(k);
}

function LastAccumulator(a: number[], k: number) {
    return last(a) === k;
}

function UniqAccumulator(a: number[], k: number) {
    return !a.includes(k);
}

function LastUniqAccumulator(a: number[], k: number) {
    return last(a) !== k;
}

const CALCULATORS: Readonly<{ [_ in RuleType]: (draws: number[], dict: Map<number, number[]>, rule: Rule, interval: Interval) => IterableIterator<{ draw: number, data: string }> }> = Object.freeze({
    ['Повтор']: accCalc.bind(Accumulator),
    ['Не выпало']: accCalc.bind(UniqAccumulator),
    ['Четные']: calc.bind((a: number[]) => a.every(n => n % 2 === 0)),
    ['Нечетные']: calc.bind((a: number[]) => a.every(n => n % 2 === 1)),
    ['Одинаковых <']: calc.bind((a: number[], v: number) => addOne(a.length - new Set(a).size) < v),
    ['Одинаковых >']: calc.bind((a: number[], v: number) => addOne(a.length - new Set(a).size) > v),
    ['Есть соседи']: calc.bind((a: number[]) => a.sort((a, b) => a - b).some((v, i) => Math.abs(v - a[i - 1]) === 1)),
    ['Нет соседей']: calc.bind((a: number[]) => !a.sort((a, b) => a - b).some((v, i) => Math.abs(v - a[i - 1]) === 1)),
    ['MIN <']: calc.bind((a: number[], v: number) => Math.min(...a) < v),
    ['MIN >']: calc.bind((a: number[], v: number) => Math.min(...a) > v),
    ['MAX <']: calc.bind((a: number[], v: number) => Math.max(...a) < v),
    ['MAX >']: calc.bind((a: number[], v: number) => Math.max(...a) > v),
    ['Сумма <']: calc.bind((a: number[], v: number) => a.reduce((sum, cv) => sum + cv, 0) < v),
    ['Сумма >']: calc.bind((a: number[], v: number) => a.reduce((sum, cv) => sum + cv, 0) > v),
    ['Есть кратный']: calc.bind((a: number[], v: number) => a.some(i => i % v === 0)),
    ['Нет кратного']: calc.bind((a: number[], v: number) => !a.some(i => i % v === 0)),
    ['MIN четный']: calc.bind((a: number[]) => Math.min(...a) % 2 === 0),
    ['MAX четный']: calc.bind((a: number[]) => Math.max(...a) % 2 === 0),
    ['MIN нечетный']: calc.bind((a: number[]) => Math.min(...a) % 2 === 1),
    ['MAX нечетный']: calc.bind((a: number[]) => Math.max(...a) % 2 === 1),
    ['Разность >']: calc.bind((a: number[], v: number) => Math.max(...a) - Math.min(...a) > v),
    ['Разность <']: calc.bind((a: number[], v: number) => Math.max(...a) - Math.min(...a) < v),
    //
    ['Четный (бонус)']: calc.bind((a: number[]) => last(a) % 2 === 0),
    ['Нечетный (бонус)']: calc.bind((a: number[]) => last(a) % 2 !== 0),
    ['Повтор (бонус)']: accCalc.bind(LastAccumulator),
    ['Не выпало (бонус)']: accCalc.bind(LastUniqAccumulator)
});

const message = require('./event.mustache');

@Singleton
export class SiteClient {
    lotteries = new Set<LotteryType>();
    cache: { [_ in LotteryType]?: Map<number, number[]> } = Object.create(null);

    constructor(
        @Inject private readonly botSettings: BotSettings,
        @Inject private readonly rulesRepository: RuleRepository,
        @Inject private readonly bot: Bot
    ) { }

    async actualizeTypesList() {
        this.lotteries = new Set((await this.rulesRepository.find()).map(v => v.lottery));
        for (let lottery in this.cache) {
            if (!this.lotteries.has(lottery as LotteryType)) {
                this.cache[lottery] = null;
            }
        }
    }

    async process() {
        try {
            await this.actualizeTypesList();
        } catch (err) {
            LOG_ERROR(err);
        }
        for (; ;) {
            const { isReady } = this.bot;
            if (isReady) {
                for (let lottery of this.lotteries) {
                    try {
                        const rules = await this.rulesRepository.find({ lottery });
                        if (rules.length > 0) {

                            const dict = this.cache[lottery] || (this.cache[lottery] = new Map());

                            let page = 0;

                            const sizeBefore = dict.size;
                            do {
                                const d = new Date(Date.now() - 86400 * 1000 * 2);//2days
                                const { body } = await promiseRequest('POST', {
                                    url: `https://www.stoloto.ru/draw-results/${lottery}/load`,
                                    headers: {
                                        Host: 'www.stoloto.ru',
                                        Origin: 'https://www.stoloto.ru',
                                        Referer: `https://www.stoloto.ru/${lottery}/archive`,
                                        'X-Requested-With': 'XMLHttpRequest'
                                    },
                                    form: {
                                        page: ++page,
                                        searchBy: 'date',
                                        withSuperprize: false,
                                        from: `${d.getUTCDate()}.${d.getUTCMonth() + 1}.${d.getUTCFullYear()}`,
                                        to: '01.01.2038'
                                    }
                                });
                                const { data } = JSON.parse(body);
                                cheerio.load(data, { decodeEntities: false })('.numbers').each(function () {
                                    const self = cheerio(this);
                                    const text = self.prev('.draw').text();
                                    const d = parseInt(text);
                                    if (d !== d) {
                                        LOG_2ERRORS(lottery, text);
                                    } else {
                                        const ns: number[] = <any>self
                                            .children('.numbers_wrapper')
                                            .eq(0)
                                            .find('b')
                                            .map(function () { return parseInt(cheerio(this).text()) })
                                            .toArray();

                                        if (Array.isArray(ns) && ns.length > 0) {
                                            dict.set(d, ns);
                                        }
                                    }
                                });
                            } while (dict.size < MAX_PROCESSED_LOTTERIES && page < 2);

                            if (dict.size > sizeBefore && dict.size > 0) {
                                const draws = Array.from(dict.keys()).sort((a, b) => b - a);

                                for (let rule of rules) {
                                    let lastProcessedDraw = rule.lastProcessedDraw;
                                    let alerts = '';
                                    for (let info of CALCULATORS[rule.type](
                                        draws,
                                        dict,
                                        rule,
                                        rule.type.endsWith('(бонус)') ? { min: 1, max: 4 } : INTERVALS[lottery]
                                    )) {
                                        if (info.draw <= lastProcessedDraw) {
                                            break;
                                        }
                                        lastProcessedDraw = Math.max(info.draw, lastProcessedDraw);
                                        let title = rule.type;
                                        if (title.endsWith('<') || title.endsWith('>') || title.includes('кратн')) {
                                            title += ' ' + rule.value;
                                        }
                                        alerts += message(Object.assign(info, {
                                            lottery,
                                            lotteryFormatted: formatType(lottery),
                                            title
                                        })) + '\n';
                                        if (alerts.length > 2500) {
                                            await this.bot.alert(alerts);
                                            alerts = '';
                                        }
                                    }
                                    if (alerts.length > 0) {
                                        await this.bot.alert(alerts);
                                    }
                                    if (lastProcessedDraw > rule.lastProcessedDraw) {
                                        rule.lastProcessedDraw = lastProcessedDraw;
                                        await this.rulesRepository.save(rule, { reload: false, listeners: false });
                                    }
                                }

                                if (draws.length > MAX_PROCESSED_LOTTERIES * 2) {
                                    const min = draws[MAX_PROCESSED_LOTTERIES * 2 - 1];
                                    for (let k of dict.keys()) {
                                        if (k < min) {
                                            dict.delete(k);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (err) {
                        LOG_ERROR(err);
                    } finally {
                        await fastDelay(this.botSettings.minimumUpdateInterval * 1000);
                    }
                }
            }
            if (this.lotteries.size < 1 || !isReady) {
                await fastDelay(this.botSettings.minimumUpdateInterval * 1000);
            }
        }
    }
}