import { Inject, Singleton } from 'typescript-ioc';

import { BotSettings } from '@entities/BotSettings';
import { HOOKS } from '@utils/config';
import { toRouter } from '@utils/routes-helpers';
import { sendErrorEmail } from '@utils/send-email';

import { Bot } from './Bot';
import { MainRouter } from './core/MainRouter';
import { SiteClient } from './SiteClient';

@Singleton
export class App {
    constructor(
        @Inject private readonly botSettings: BotSettings,
        @Inject private readonly client: SiteClient,
        @Inject private readonly bot: Bot,
        @Inject router: MainRouter
    ) {
        const botRouter = toRouter(bot);
        router.use(HOOKS, botRouter.routes(), botRouter.allowedMethods());
    }

    async init() {
        if (this.botSettings.telegramBotToken) {
            try {
                await this.bot.update(this.botSettings.telegramBotToken);
            } catch (err) {
                await sendErrorEmail('Ошибка запуска бота. Проверьте токен телеграм бота.');
                throw err;
            }
        }
        this.client.process().catch(err => LOG_ERROR(err));
    }
}