import { EntitySubscriberInterface, EventSubscriber, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { BotSettings } from '@entities/BotSettings';

import { Bot } from '../Bot';

@EventSubscriber()
export class BotSettingsSubscriber implements EntitySubscriberInterface<BotSettings> {

    listenTo() {
        return BotSettings;
    }

    async beforeUpdate({ entity }: UpdateEvent<BotSettings>) {
        try {
            await (Container.get(Bot) as Bot).update(entity.telegramBotToken);
        } catch (err) {
            LOG_ERROR(err);
            throw [{
                field: 'telegramBotToken',
                message: 'Invalid telegram token'
            }];
        }
    }
}