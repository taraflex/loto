import { EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { Rule } from '@entities/Rule';

import { SiteClient } from '../SiteClient';

@EventSubscriber()
export class RulesSubscriber implements EntitySubscriberInterface<Rule> {

    listenTo() {
        return Rule;
    }

    beforeInsert({ entity }: InsertEvent<Rule>) {
        if (entity) {
            if (entity.type.endsWith('(бонус)')) {
                entity.lottery = 'rapido';
            }
            if (entity.type.startsWith('Повтор') || entity.type.startsWith('Не выпало')) {
                entity.count = Math.max(2, entity.count);
            }
        }
    }

    beforeUpdate({ entity }: UpdateEvent<Rule>) {
        if (entity) {
            if (entity.type.endsWith('(бонус)')) {
                entity.lottery = 'rapido';
            }
            if (entity.type.startsWith('Повтор') || entity.type.startsWith('Не выпало')) {
                entity.count = Math.max(2, entity.count);
            }
        }
    }

    async afterInsert(_) {
        return (Container.get(SiteClient) as SiteClient).actualizeTypesList();
    }

    async afterUpdate(_) {
        return (Container.get(SiteClient) as SiteClient).actualizeTypesList();
    }

    async afterRemove(_) {
        return (Container.get(SiteClient) as SiteClient).actualizeTypesList();
    }
}