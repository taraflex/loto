import * as TelegramBot from 'node-telegram-bot-api';
import { Inject, Singleton } from 'typescript-ioc';

import { Settings, SettingsRepository } from '@entities/Settings';
import { BaseTelegramBot } from '@utils/BaseTelegramBot';

@Singleton
export class Bot extends BaseTelegramBot {

    constructor(
        @Inject protected readonly settings: Settings,
        @Inject protected readonly settingsRepository: SettingsRepository,
    ) {
        super();
        this.allowedUpdates.add('message');
    }

    async onMessage(m: TelegramBot.Message) {
        try {
            if (this.settings.admin === 0) {
                this.settings.admin = m.from.id;
                await this.settingsRepository.save(this.settings);
                await this.alert('Регистрация администратора прошла успешно 🎉');
            }
        } catch (err) {
            LOG_ERROR(err);
        }
    }

    get isReady() {
        return this.settings.admin && this.bot;
    }

    alert(text) {
        return this.settings.admin ? this.sendMessage(this.settings.admin, text, {
            parse_mode: 'Markdown',
            disable_web_page_preview: true
        }) : undefined;
    }
}